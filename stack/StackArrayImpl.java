package stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayImpl implements Stack {

	List items = new ArrayList();

	@Override
	public void push(Object obj) {
		items.add(obj);
	}

	@Override
	public Object pop() {
		items.remove(items.size() - 1);
		return null;
	}

	@Override
	public boolean empty() {
		return items.size() == 0;
	}

}
 